---
title: "Má vaše dítě nadváhu nebo je obézní? Ověřte si to v on-line kalkulačce"
perex: "Každé sedmé dítě v Česku trpí nadváhou, u dospívajících je to každý čtvrtý, mezi dospělými každý druhý. Přibývá přitom těžších forem obezity, a to i u dětí."
description: "Každé sedmé dítě v Česku trpí nadváhou, u dospívajících je to každý čtvrtý, mezi dospělými každý druhý."
authors: ["Pavla Lioliasová", "Petr Kočí"]
published: "26. března 2017"
# coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
# coverimg_note: "Foto <a href='#'>ČTK</a>"
url: "obezita"
styles: ["https://interaktivni.rozhlas.cz/obezita/bootstrap/css/bootstrap.min.css","https://interaktivni.rozhlas.cz/obezita/bootstrap/css/bootstrap-theme.min.css", "https://interaktivni.rozhlas.cz/obezita/styl/kalkulacka.css"]
libraries: [highcharts, jquery, "https://interaktivni.rozhlas.cz/obezita/bootstrap/js/bootstrap.min.js"]
socialimg: https://interaktivni.rozhlas.cz/obezita/media/valecek.jpg
recommended:
  - link: https://interaktivni.rozhlas.cz/stesti/
    title: Konec blbé nálady. Češi v pocitu životního štěstí dohnali Západ
    perex: Ukázal to výzkum Evropské banky pro obnovu a rozvoj, kterého se zúčastnilo na 50 tisíc domácností ve čtyřiatřiceti zemích.
    image: https://interaktivni.rozhlas.cz/stesti/media/socimg.jpg
  - link: http://www.rozhlas.cz/zpravy/data/_zprava/leky-v-datech-na-cem-jsme-zavisli-kolik-utracime--1465581
    title: Léky v datech: Na čem jsme závislí, kolik utrácíme?
    perex: Léků bereme rok od roku víc. Nejde ovšem jen o mediálně propíraná antibiotika nebo vakcíny. Rychleji dnes roste spotřeba antidepresiv, prášků na srdce nebo léků na rakovinu. 
    image: http://media.rozhlas.cz/_obrazek/2810684--laborator--1-950x0p0.jpeg
  - link: https://interaktivni.rozhlas.cz/umrti-srdce/
    title: Na co se kde v Evropě nejčastěji umírá? Podívejte se na podrobnou mapu
    perex: Češi mají díky lékařské péči největší šanci na přežití srdečního infarktu na světě. Ani to ale nestačí: počtem úmrtí na nemoci srdce patříme do méně civilizované části Evropy. Umíráme na ně třikrát častěji než Francouzi.
    image: https://interaktivni.rozhlas.cz/umrti-srdce/media/socimg.png
---
	
<aside class="small">
	<h3>Netrpí vaše dítě nadváhou?</h3>
	<figure>
		<div class="container-fluid well" id="kalkulacka">
			<div class="btn-group btn-toggle form-group"> 
				<button class="btn btn-default">Chlapec</button>
				<button class="btn btn-primary active">Dívka</button>
			</div>
			<div class=form-group>
				<label for="vek">Věk (od 5 do 18 let)</label> 	
				<input class="form-control" id="vek" type="number" value="7" min="5" max="18" step="0.5">
			</div>
			<div class=form-group>
				<label for="vyska">Výška (cm)</label> 	
				<input class="form-control" id="vyska" type="number" value="124" max="230">
			</div>
			<div class=form-group>
				<label for="vaha">Váha (kg)</label> 	
				<input class="form-control" id="vaha" type="number" value="24" max="160">
			</div>
			<button type="submit" class="btn btn-success" id="spocti">Spočítat</button>			
		</div>
		</figure>
		<figcaption>
			Zdroj dat: <a href="http://www.szu.cz/uploads/documents/czzp/BMI_a_05_roku.pdf">Státní zdravotní ústav</a>, výpočet ČRo
		</figcaption>
</aside>

Rozšířený názor, že přibývá dětí, které se málo hýbou, hůř jedí, a přibývají proto na váze, není snadné doložit. [Celostátní antropologické výzkumy](http://www.szu.cz/publikace/data/celostatni-antropologicke-vyzkumy-cav), které prováděli lékaři plošně každé desetiletí od padesátých let, ukončilo ministerstvo zdravotnictví v roce 2001 – tím že na měření v roce 2011 neposkytlo peníze.

Nasbíraná data z předchozích desetiletí obezitologové dodnes [zkoumají](http://www.szu.cz/uploads/documents/obi/CAV/6.CAV_3_Zmeny_telesnych_proporc.pdf) a používají mj. pro určení toho, kde hranice nadváhy či obezity vlastně začíná. Nadváhou trpí podle [oficiální definice](http://www.szu.cz/publikace/data/detska-obezita) ten, kdo má [index tělesné hmotnosti (BMI)](https://cs.wikipedia.org/wiki/Index_t%C4%9Blesn%C3%A9_hmotnosti) vyšší než 90 procent české populace stejného pohlaví v daném věku v roce 1991. Obézní je pak ten, kdo by při měření a vážení v roce 1991 spadl mezi tři procenta věkově blízkých lidí s nejvyšším BMI.

Na stejném principu funguje naše interaktivní kalkulačka: Můžete si v ní ověřit, do které kategorie při použití této metody patří děti a mladiství mezi 5 a 18 lety věku.

## Nadváha a obezita stoupá s věkem

"Dobrá data máme například o sedmiletých dětech," vysvětlila Českému rozhlasu obezitoložka Marie Kunešová z [Endokrinologického ústavu](http://web2.endo.cz/cz/index.php/organizacni-struktura/jednotliva-oddeleni/klinicka-oddeleni/oddeleni-obezitologie-obe/).

Na nich je podle ní vidět trend, který se ještě výrazněji projevuje u adolescentů a dospělých: Zatímco celkové procento lidí s nadváhou a obezitou zůstává v populaci v posledních letech přibližně stejné, přibývá mezi nimi těch, kdo trpí těžšími formami obezity.

<div data-bso="1" id="graf1" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

"Vzestup vyšších stupňů obezity jednoznačně vidíme, například právě u sedmiletých děvčat," říká obezitoložka. "Populace se nám dělí. První část lépe reaguje na různé preventivní programy, tam dochází k ústupu nadváhy. Jsou ale části dětské populace, které na prevenci ani léčbu nereagují, a u nich právě dochází k vzestupu těžších stupňů."
 
Podle obezitoložky je klíčová rodinná anamnéza: U dětí a adolescentů, jejichž oba rodiče trpí nadváhou, je výskyt nadváhy až pětkrát častější: "Nejedná se jenom o genetické faktory, ale i o zvyky v rodině. Jak kdo jí a jak se pohyuje. Když dítě jezdí od malička s rodiči na výlety, na hory a chodí sportovat, pokračuje pak v těchto aktivitách obvykle i ve vyšším věku. Pokud ale tyto aktivity v rodině nevidí, sedí u televize nebo u počítače, jsou efekty opačné."

Ve srovnání s ostatními evropskými zeměmi na tom nejsou české děti špatně: Nadváhou či obezitou jich trpí zhruba patnáct procent. U adolescentů už je to 25 procent a u dospělých téměř polovina populace.