require! {
  yamljs
  fs
  marked
  diacritics
  "./utils"
}
module.exports = (cb) ->
  raw = fs.readFileSync "#__dirname/../article.md"
    .toString!
  [_, headerRaw, ...rest] = raw.split /[-]{3,}/
  articleRaw = rest.join "---"
  header = yamljs.parse headerRaw
  renderer = new marked.Renderer!

  renderer.heading = (text, level, raw) ->
    escapedText = (diacritics.remove raw).toLowerCase().replace(/[^\w]+/g, '-')
    escapedText .= replace /^-+|-+$/ ''
    """<h#{level} id="#{@options.headerPrefix}#escapedText">#text</h#{level}>\n"""
  content = marked articleRaw, {renderer}
  template = fs.readFileSync "#__dirname/../template/template.html"
    .toString!
  [..., projectDir, _] = __dirname.split /[\\/]/
  canonical = if header.url
    "<link rel='canonical' href='https://interaktivni.rozhlas.cz/#{header.url}/'>"
  else
    ""
  url =
    | header.url
      "https://interaktivni.rozhlas.cz/" + header.url
    | otherwise
      "https://interaktivni.rozhlas.cz/data/#projectDir/www"

  coverimg_social = ""
  if header.socialimg
    socialimg = header.socialimg
    coverimg_social = "
        <meta name='twitter:image' content='#{socialimg}'>
        <meta property='og:image' content='#{socialimg}'>
        <meta itemprop='image' content='#{socialimg}'>
      "
  if header.coverimg
    images = []
    images.push "media/" + utils.processImage header.coverimg, {width: 1920, quality: 80}
    images.push "media/" + utils.processImage header.coverimg, {width: 1600, quality: 80}
    images.push "media/" + utils.processImage header.coverimg, {width: 1024, quality: 75}
    images.push "media/" + utils.processImage header.coverimg, {width: 600, quality: 60}
    images.push "media/" + utils.processImage header.coverimg, {width: 450, quality: 60}
    coverimg = "<img src='#{images[1]}' alt='' srcset='#{images[0]} 1920w,#{images[1]} 1600w,#{images[2]} 1024w,#{images[3]} 600w,,#{images[4]} 450w'>"
    if header.coverimg_note then coverimg += "<span class='note'>#that</span>"
    if not coverimg_social
      coverimg_social = "
        <meta name='twitter:image' content='#{url}/#{images[0]}'>
        <meta property='og:image' content='#{url}/#{images[0]}'>
        <meta itemprop='image' content='#{url}/#{images[0]}'>
      "
    nocoverimgclass = ""
  else
    coverimg = ""
    nocoverimgclass = "class='no-cover-img'"

  scripts =
    | header.libraries
      header.libraries
        .map libToScript
        .join ""
    | otherwise
      ""


  styles =
    | header.styles
      stylesToHtml header.styles
    | otherwise
      ""

  recommended = ""
  if header.recommended
    recommended = "<aside class='recommended'>"
    recommended += "<h1>Čtěte také</h1>"
    recommended += "<ul>"
    recommended += that
      .filter -> it.link && it.image && it.title
      .map ->
        image = "https://interaktivni.rozhlas.cz/obezita/media/" + utils.processImage it.image, {width: 320, quality: 80}
        "<li><a href='#{it.link}' target='_blank'><div class='img'><img src='#{image}' alt='#{it.title}'></div><div class='right'><h2>#{it.title}</h2><p>#{it.perex}</p></div></a></li>"
      .join ""
    recommended += "</ul></aside>"


  mapping =
      "TITLE": header.title
      "PEREX": header.perex
      "PUBLISHED": header.published
      "AUTHORS": utils.toAuthors header.authors
      "YEAR": new Date!getFullYear!
      "DESCRIPTION": header.description || header.perex
      "CONTENT": content
      "URL": url
      "STYLES": styles
      "SCRIPTS": scripts
      "COVERIMG": coverimg
      "COVERIMG_SOCIAL": coverimg_social
      "NOCOVERIMGCLASS": nocoverimgclass
      "CANONICAL": canonical
      "RECOMMENDED": recommended


  for field, replacement of mapping
    rx = new RegExp "\\$\\$" + field + "\\$\\$", "gi"
    template .= replace rx, replacement

  fs.writeFileSync "#__dirname/../www/_index.html", template
  fs.writeFileSync "#__dirname/../www/rscr-redirect.js", "window.location.href='#url/'"
  cb?!

librariesAssoc =
  "d3"           : \https://interaktivni.rozhlas.cz/tools/d3/3.5.3.min.js
  "topojson"     : \https://interaktivni.rozhlas.cz/tools/topojson/1.6.8.min.js
  "jquery"       : \https://interaktivni.rozhlas.cz/tools/jquery/2.1.1.min.js
  "highcharts"   : \https://interaktivni.rozhlas.cz/tools/highcharts/4.2.5.js
  "leaflet"      : \https://interaktivni.rozhlas.cz/tools/leaflet/0.7.3.cssjs.js
  "inline-audio" : \https://interaktivni.rozhlas.cz/tools/inline-audio/0.0.2.js


libToScript = (addr) ->
  if librariesAssoc[addr]
    """<script src="#{librariesAssoc[addr]}"></script>"""

  else
    """<script src="#addr"></script>"""

stylesToHtml = (stylesArray) ->
  stylesJson = JSON.stringify stylesArray
  "<script>window.requestAnimationFrame(function(){var he=document.getElementsByTagName('head')[0];#{stylesJson}.forEach(function(hr){var s=document.createElement('link');s.href=hr;s.rel='stylesheet';s.type='text/css';he.appendChild(s)})})</script>"
