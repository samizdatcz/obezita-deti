Highcharts.chart('graf1', {
    colors: ['#fff7bc','#fec44f','#d95f0e'],
    chart: {
        type: 'column'
    },
    title: {
        text: 'Nadváha a obezita u sedmiletých dětí v České republice'
    },
    xAxis: {
        categories: ['Chlapci s nadváhou', 'Obézní chlapci', 'Dívky s nadváhou', 'Obézní dívky']
    },
    yAxis: {
        title: {
            text: "procento populace"    
        } 
    },  
    tooltip: {
        valueSuffix: "% z populace"
    },
    credits: {
        text: "Zdroj: Childhood Obesity Surveillance Initiative",
        href: "http://www.euro.who.int/en/health-topics/disease-prevention/nutrition/activities/monitoring-and-surveillance/who-european-childhood-obesity-surveillance-initiative-cosi"
    },
    series: [{
        name: '2008',
        data: [7.6, 7.2, 6.10, 5.00]
    }, {
        name: '2010',
        data: [6.45, 6.85, 8.04, 6.92]
    }, {
        name: '2013',
        data: [6.84, 7.56, 5.59, 7.65]
    }]
});